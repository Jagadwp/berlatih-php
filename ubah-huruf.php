<?php

   function ubah_huruf($string) 
   {
      $output = "";

      for ($i = 0; $i < strlen($string); $i++) {
         if(ctype_alpha($string[$i])) {
            $ascii = ord($string[$i]);

            if ($ascii == 90 || $ascii == 122) {
               $string[$i] = chr($ascii - 25); ;
            } else {
               $ascii += 1;
               $string[$i] = chr($ascii);
            }
            $output .= $string[$i];
         } else {
            $output .= $string[$i];
         }
      }  
      return $output . "<br>";
   }

   // TEST CASES
   echo ubah_huruf('wow'); // xpx
   echo ubah_huruf('developer'); // efwfmpqfs
   echo ubah_huruf('laravel'); // mbsbwfm
   echo ubah_huruf('keren'); // lfsfo
   echo ubah_huruf('semangat'); // tfnbohbu
